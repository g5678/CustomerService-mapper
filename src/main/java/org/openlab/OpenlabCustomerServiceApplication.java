package org.openlab;

import org.openlab.dto.CustomerRequestDTO;
import org.openlab.services.CustomerService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication()
public class OpenlabCustomerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(OpenlabCustomerServiceApplication.class, args);
	}

	@Bean
	CommandLineRunner start(CustomerService customerService) {
		return arg -> {
			customerService.save(new CustomerRequestDTO("CI23", "SASASA", "exemple@com.fr"));
			customerService.save(new CustomerRequestDTO("LA52", "LakHt", "kaja@go.fr"));
			customerService.save(new CustomerRequestDTO("75KL", "Batryi", "exle@frna.fr"));
		};
	}

}
