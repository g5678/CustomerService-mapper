package org.openlab.services;

import java.util.List;

import org.openlab.dto.CustomerRequestDTO;
import org.openlab.dto.CustomerResponseDTO;

public interface CustomerService {

	CustomerResponseDTO getCustomer(String id);

	List<CustomerResponseDTO> listCustomers();

	CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO);

	CustomerResponseDTO update(CustomerRequestDTO customerRequestDTO);

	void deleteCustomer(String id);

}
