package org.openlab.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.openlab.dto.CustomerRequestDTO;
import org.openlab.dto.CustomerResponseDTO;
import org.openlab.entities.Customer;
import org.openlab.mappers.CustomerMapper;
import org.openlab.repositories.CustomerRepository;
import org.springframework.stereotype.Service;

@Transactional
@Service
public class CustomerServiceImpl implements CustomerService {

	private CustomerRepository customerRepository;
	private CustomerMapper customerMapper;

	public CustomerServiceImpl(CustomerRepository customerRepository, CustomerMapper customerMapper) {
		this.customerRepository = customerRepository;
		this.customerMapper = customerMapper;
	}

	@Override
	public CustomerResponseDTO save(CustomerRequestDTO customerRequestDTO) {

		/*
		 * Customer customer = new Customer();
		 * customer.setId(customerRequestDTO.getId());
		 * customer.setName(customerRequestDTO.getName());
		 * customer.setMail(customerRequestDTO.getMail());
		 */

		Customer customer = customerMapper.customerRequestToCustomer(customerRequestDTO);

		// customer.setId(UUID.randomUUID().toString());
		Customer saveCustomer = customerRepository.save(customer);

		/*
		 * CustomerResponseDTO customerResponseDTO = new CustomerResponseDTO();
		 * customerResponseDTO.setId(saveCustomer.getId());
		 * customerResponseDTO.setName(saveCustomer.getName());
		 * customerResponseDTO.setMail(saveCustomer.getMail());
		 */
		CustomerResponseDTO customerResponseDTO = customerMapper.customerToCustomerResponseDTO(saveCustomer);

		return customerResponseDTO;
	}

	@Override
	public CustomerResponseDTO getCustomer(String id) {
		Customer customer = customerRepository.findById(id).get();
		return customerMapper.customerToCustomerResponseDTO(customer);
	}

	@Override
	public CustomerResponseDTO update(CustomerRequestDTO customerRequestDTO) {
		Customer customer = customerMapper.customerRequestToCustomer(customerRequestDTO);
		Customer updateCustomer = customerRepository.save(customer);
		return customerMapper.customerToCustomerResponseDTO(updateCustomer);
	}

	@Override
	public List<CustomerResponseDTO> listCustomers() {
		List<Customer> customers = customerRepository.findAll();
		List<CustomerResponseDTO> listToCustomerResponse = customers.stream()
				.map(cust -> customerMapper.customerToCustomerResponseDTO(cust)).collect(Collectors.toList());
		return listToCustomerResponse;
	}

	@Override
	public void deleteCustomer(String id) {
		customerRepository.deleteById(id);
	}

}
