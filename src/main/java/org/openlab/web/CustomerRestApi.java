package org.openlab.web;

import java.util.List;
import java.util.UUID;

import org.openlab.dto.CustomerRequestDTO;
import org.openlab.dto.CustomerResponseDTO;
import org.openlab.services.CustomerService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/api")
public class CustomerRestApi {

	private CustomerService customerService;

	public CustomerRestApi(CustomerService customerService) {
		super();
		this.customerService = customerService;
	}

	@GetMapping(path = "/customers")
	public List<CustomerResponseDTO> allCustomers() {
		return customerService.listCustomers();
	}

	@PostMapping(path = "/customer")
	public CustomerResponseDTO save(@RequestBody CustomerRequestDTO customerRequestDTO) {
		customerRequestDTO.setId(UUID.randomUUID().toString());
		return customerService.save(customerRequestDTO);
	}

	@GetMapping(path = "/customer/{id}")
	public CustomerResponseDTO getCustomer(@PathVariable("id") String id) {
		return customerService.getCustomer(id);
	}

	@PutMapping(path = "/customer/{id}")
	public CustomerResponseDTO updateCustomer(@RequestBody CustomerRequestDTO customerRequestDTO,
			@PathVariable("id") String id) {
		customerRequestDTO.setId(id);
		return customerService.update(customerRequestDTO);
	}

	@DeleteMapping(path = "/customer/{id}")
	public void deleteCustomer(@PathVariable("id") String id) {
		customerService.deleteCustomer(id);
	}

}
